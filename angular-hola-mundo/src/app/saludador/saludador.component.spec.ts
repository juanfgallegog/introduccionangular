import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SaludadorComponent } from './saludador.component';

describe('SaludadorComponent', () => {
  let component: SaludadorComponent;
  let fixture: ComponentFixture<SaludadorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SaludadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaludadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
