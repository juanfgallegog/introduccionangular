import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Ingrediente } from '../models/ingrediente.model';

@Component({
  selector: 'app-ingrediente',
  templateUrl: './ingrediente.component.html',
  styleUrls: ['./ingrediente.component.css']
})
export class IngredienteComponent implements OnInit {
  @Input() ingrediente: Ingrediente;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit(): void {
  }

}
