import { Component, OnInit } from '@angular/core';
import { Ingrediente } from "../models/ingrediente.model";

@Component({
  selector: 'app-receta',
  templateUrl: './receta.component.html',
  styleUrls: ['./receta.component.css']
})
export class RecetaComponent implements OnInit {
  ingredientes: Ingrediente[];
  constructor() {
    this.ingredientes = [];
   }
  guardar(nombre: string, cantidad: string, url: string) :boolean {
    this.ingredientes.push(new Ingrediente(nombre, cantidad, url));
    console.log( this.ingredientes );
    return false;
  }
  ngOnInit(): void {
  }

}
