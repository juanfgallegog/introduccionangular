export class Ingrediente {
  nombre: string;
  cantidad: string;
  imagenUrl: string;

  constructor( n: string, c: string, u:string) {
      this.nombre = n;
      this.cantidad = c;
      this.imagenUrl = u;
  }
}
