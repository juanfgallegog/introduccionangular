import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RecetaComponent } from './receta/receta.component';
import { IngredienteComponent } from './ingrediente/ingrediente.component';

@NgModule({
  declarations: [
    AppComponent,
    RecetaComponent,
    IngredienteComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
